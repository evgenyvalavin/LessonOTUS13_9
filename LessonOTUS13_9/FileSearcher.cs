﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS13_9
{
    public class FileSearcher
    {
        public event EventHandler<string> FileFound;
        public static bool IsCanselled = false;

        public void SearchNewFiles()
        {
            var files = Directory.EnumerateFiles(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "*.*");
            foreach (var file in files)
            {
                if (IsCanselled)
                    break;
                FileFound?.Invoke(this, file);
            }
        }
    }
}
