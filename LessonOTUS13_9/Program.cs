﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LessonOTUS13_9
{
    public static class Program
    {
        public static T GetMax<T>(this List<T> collection, Func<T, int> func)
        {
            var result = collection[0];
            int max = int.MinValue;
            foreach (var item in collection)
            {
                int current = func(item);
                if (current > max)
                {
                    max = current;
                    result = item;
                }
            }
            return result;
        }

        public static List<Point> points = new List<Point>()
        {
            new Point() { X = 4, Y = 12 },
            new Point() { X = 2, Y = 41 },
            new Point() { X = 3, Y = 12 }
        };

        public static void Main(string[] args)
        {
            FileSearcher fileSearcher = new FileSearcher();
            fileSearcher.FileFound += OnFileFound;
            fileSearcher.SearchNewFiles();
            Point MaxPoint = points.GetMax(x => x.X + x.Y);
            Console.WriteLine("\nMax point: " + MaxPoint.ToString());
            Console.ReadKey();
        }

        private static void OnFileFound(object sender, string e)
        {
            Console.WriteLine(DateTime.Now.ToString("H:mm:sss.fff") + " " + e);
        }
    }
}
